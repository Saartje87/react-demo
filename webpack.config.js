const nodeEnv = process.env.NODE_ENV || 'dev';

module.exports = require(`./webpack/webpack.${nodeEnv}.js`);
