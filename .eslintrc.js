module.exports = {
	"env": {
		"browser": true,
	},
	"parser": "babel-eslint",
	"parserOptions": {
		"sourceType": "module",
		"allowImportExportEverywhere": false
	},
	"extends": "airbnb",
	"plugins": [
		"react",
		"jsx-a11y",
		"import"
	],
	"rules": {
		"class-methods-use-this": "off",
		"import/extensions": "off",
		"import/no-extraneous-dependencies": "off",
		"import/no-unresolved": [2, { ignore: ['^(components|actions|reducers|config|containers)'] }],
		"import/prefer-default-export": "off",
		"import/first": "off",
		"indent": ["error", "tab", { "SwitchCase": 1 }],
		"no-shadow": "off",
		"max-len": "off",
		"no-tabs": "off",
		"quotes": [2, "single", { "allowTemplateLiterals": true }],
		"react/jsx-curly-spacing": [1, "always"],
		"react/jsx-indent-props": ["error", "tab"],
		"react/jsx-indent": ["error", "tab"],
		// "react/jsx-no-bind": "off",
		"react/prefer-stateless-function": "off",
		"react/no-children-prop": "off",
		"react/no-string-refs": "off",									// TODO Should be enabled
		"no-plusplus": "off",
	},
};
