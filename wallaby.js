const webpack = require('webpack');
const wallabyWebpack = require('wallaby-webpack');
const webpackDevConfig = require('./webpack/webpack.dev.js');

module.exports = (wallaby) => {
	const webpackPostprocessor = wallabyWebpack({
		devtool: 'inline-source-map',
		externals: {
		// Use external version of React instead of rebuilding it
			react: 'React',
		},
		context: webpackDevConfig.context,
		resolve: webpackDevConfig.resolve,
		module: webpackDevConfig.module,
		plugins: [
			new webpack.NormalModuleReplacementPlugin(/\.(gif|png|scss|css|mp3)$/, 'node-noop'),
		],
	});

	return {
		files: [
			{ pattern: 'node_modules/react/dist/react-with-addons.js', instrument: false },
			{ pattern: 'client/**/!(*.spec)*.js*', load: false },
		],
		tests: [
			{ pattern: 'client/**/*.spec.js*', load: false },
		],
		compilers: {
			'**/*.js*': wallaby.compilers.babel(),
		},
		postprocessor: webpackPostprocessor,
		bootstrap: () => {
			/* eslint no-underscore-dangle: "off" */
			// required to trigger test loading
			window.__moduleBundler.loadTests();
		},
	};
};
