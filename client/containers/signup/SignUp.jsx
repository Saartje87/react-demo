import React, { Component, PropTypes } from 'react';

import './signup.css';
import Input from 'components/input';

class SignUp extends Component {
	static propTypes = {
		onDone: PropTypes.func.isRequired,
	};

	state = {
		name: null,
		spinner: false,
	};

	goAction = ::this.goAction;

	goAction() {
		const {
			name,
		} = this.state;

		if (!name) {
			return;
		}

		this.setState({
			spinner: true,
		});

		// Fake request..
		setTimeout(() => {
			this.props.onDone();
			this.animateOut();
		}, 800);
	}

	animateOut() {
		const {
			self,
			button,
		} = this.refs;

		button.animate([
			{ transform: `translateY(0) scale(1)`, opacity: 1 },
			{ offset: 0.3, transform: `translateY(-20vh) scale(1)`, opacity: 1 },
			{ transform: `translateY(-20vh) scale(12)`, opacity: 1 },
		], {
			duration: 500,
			easing: 'cubic-bezier(0.23, 1, 0.32, 1)',
			fill: 'both',
		});

		const animation = self.animate([
			{ opacity: 1 },
			{ opacity: 0 },
		], {
			delay: 100,
			duration: 400,
			easing: 'cubic-bezier(0.23, 1, 0.32, 1)',
			fill: 'both',
		});

		animation.onfinish = () => { self.style.display = 'none'; };
	}

	render() {
		const {
			name,
			spinner,
		} = this.state;

		return (
			<div ref="self" className="signup">
				<h1>Sign up</h1>

				<p>Please fill in your username</p>

				<Input ref="name" label="Username" onChange={ value => this.setState({ name: value }) } />

				<br />

				<div
					ref="button"
					disabled={ !name || spinner }
					tabIndex="-1"
					className={ `signup--button ${spinner ? 'signup--button-is-spinning' : ''}` }
					onClick={ this.goAction }
				>
					Go
				</div>
			</div>
		);
	}
}

export default SignUp;
