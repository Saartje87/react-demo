import React, { Component, PropTypes } from 'react';
import {
	HashRouter as Router,
	Link,
	Match,
	Miss,
	Redirect,
} from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import './app.css';
import SignUp from '../signup';
import Swipe from 'components/swipe';
import Footer from 'components/footer';
import Page from 'components/page';
import Interaction from 'components/interaction';
import About from 'components/about';
import {
	submitAnswer,
} from 'actions/interaction';
import { Tabs, Tab } from 'components/tabs';

@connect(
	({ interaction }) => ({ interaction }),
	dispatch => bindActionCreators({ submitAnswer }, dispatch),
)
class App extends Component {
	static propTypes = {
		interaction: PropTypes.shape({
			interactions: PropTypes.arrayOf(PropTypes.shape({
				id: PropTypes.number.isRequired,
				title: PropTypes.string.isRequired,
			})),
		}),
		submitAnswer: PropTypes.func.isRequired,
	};

	state = {
		index: 0,
		registered: true,
	};

	render() {
		const {
			interaction,
			submitAnswer,
		} = this.props;
		const {
			registered,
		} = this.state;

		return (
			<Router>
				{/* jsx style comment */}
				<div className="app">
					{/* More contents of our app will be 'preloaded' when the user is registered */}
					{/* registered &&
						<div className="app--container">
							<Swipe className="app--body">
								<Match exactly pattern="/">
									{ props => (
										<Page { ...props }>
											<Interaction interaction={ interaction } submitAnswer={ submitAnswer } />
										</Page>
									) }
								</Match>
								<Match pattern="/about">
									{ props => (
										<Page { ...props }>
											<About />
										</Page>
									) }
								</Match>
							</Swipe>
							<Footer>
								<Link className="footer--link" to="/">Interaction</Link>
								<Link className="footer--link" to="/about">About</Link>
							</Footer>
						</div> */}

					{/* TODO Implement routing to tabs
					<Match
						pattern="/"
						render={ props => <span>{ props.isExact && this.setState({ index: 0 }) }</span> }
					/>*/}

					{ registered &&
						<Tabs className="app-tabs" index={ this.state.index } onIndexChange={ index => this.setState({ index }) }>
							<Tab label="Lobby">
								<Page>
									<Interaction interaction={ interaction } submitAnswer={ submitAnswer } />
								</Page>
							</Tab>
							<Tab label={ <span>Leaders</span> }>
								<Page>
									<About />
								</Page>
							</Tab>
							<Tab label="Predictions">
								<Page>
									<h1>Predictions</h1>
								</Page>
							</Tab>
							<Tab label="Friends">
								<Page>
									<h1>Friends</h1>
								</Page>
							</Tab>
							<Tab label="More">
								<Page>
									<h1>More</h1>
								</Page>
							</Tab>
						</Tabs> }

					{/* Sign up layer */}
					{/*<SignUp onDone={ () => this.setState({ registered: true }) } />*/}

					<Miss render={ () => <Redirect to="/" /> } />
				</div>
			</Router>
		);
	}
}

export default App;
