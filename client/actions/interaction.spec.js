import {
	UPDATE_ANSWER,
	submitAnswer,
} from './interaction';

describe('Interaction actions', () => {
	it('should submit an answer', () => {
		const payload = { id: 1, title: 'Foo' };
		const expectedAction = {
			type: UPDATE_ANSWER,
			payload,
		};

		expect(submitAnswer(payload))
			.toEqual(expectedAction);
	});
});
