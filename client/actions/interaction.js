export const UPDATE_ANSWER = '@interaction/UPDATE_ANSWER';

export const submitAnswer = payload => ({
	type: UPDATE_ANSWER,
	payload,
});
