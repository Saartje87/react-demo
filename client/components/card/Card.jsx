import React, { Component, PropTypes } from 'react';

import './card.css';

class Card extends Component {
	static propTypes = {
		children: PropTypes.node.isRequired,
		title: PropTypes.string.isRequired,
		onDismiss: PropTypes.func.isRequired,
		disabled: PropTypes.bool,
	};

	card = null;

	// Bind methods to instance
	update = ::this.update;
	startAction = ::this.startAction;
	moveAction = ::this.moveAction;
	endAction = ::this.endAction;

	cardBCR = null;
	currentX = null;
	dragging = null;
	screenX = null;
	startX = null;

	componentDidMount() {
		if (this.props.disabled) {
			this.node.animate([
				{ transform: `scale(0)` },
				{ transform: `scale(1)` },
			], {
				duration: 500,
				easing: 'cubic-bezier(0.23, 1, 0.32, 1)',
				fill: 'both',
			});
		}
	}

	addListeners() {
		document.addEventListener('touchmove', this.moveAction, true);
		document.addEventListener('touchend', this.endAction);

		document.addEventListener('mousemove', this.moveAction);
		document.addEventListener('mouseup', this.endAction);
	}

	removeListeners() {
		document.removeEventListener('touchmove', this.moveAction, true);
		document.removeEventListener('touchend', this.endAction);

		document.removeEventListener('mousemove', this.moveAction);
		document.removeEventListener('mouseup', this.endAction);
	}

	startAction(event) {
		if (this.props.disabled) {
			return;
		}

		event.preventDefault();

		requestAnimationFrame(this.update);

		this.cardBCR = this.card.getBoundingClientRect();
		this.startX = event.pageX || event.touches[0].pageX;
		this.currentX = this.startX;

		this.dragging = true;
		this.card.style.willChange = 'transform';

		this.addListeners();
	}

	moveAction(event) {
		event.preventDefault();

		this.currentX = event.pageX || event.touches[0].pageX;
	}

	endAction() {
		this.dragging = false;

		this.targetX = 0;
		const screenX = this.currentX - this.startX;
		const threshold = this.cardBCR.width * 0.35;

		this.removeListeners();

		if (Math.abs(screenX) > threshold) {
			this.targetX = (screenX > 0) ?
				this.cardBCR.width :
				-this.cardBCR.width;
		}
	}

	update() {
		const prevScreenX = Math.round(this.screenX);

		if (this.dragging) {
			this.screenX = this.currentX - this.startX;
		} else {
			this.screenX += (this.targetX - this.screenX) / 4;
		}

		// Bail out when leave animation has finished
		if (!this.card) {
			return;
		}

		if (!this.dragging && prevScreenX === Math.round(this.screenX)) {
			const foo = Math.abs(this.screenX) > (this.cardBCR.width - 10);

			this.card.style.willChange = '';

			if (foo && this.props.onDismiss) {
				this.props.onDismiss((this.targetX > 0) ? 'yes' : 'no');
			}

			return;
		}

		requestAnimationFrame(this.update);

		const normalizedDragDistance = (Math.abs(this.screenX) / this.cardBCR.width);
		const opacity = 1 - (normalizedDragDistance ** 3);

		this.card.style.transform = `translateX(${this.screenX}px)`;
		this.card.style.webkitTransform = `translateX(${this.screenX}px)`;
		this.card.style.opacity = opacity;
	}

	render() {
		const {
			children,
			title,
			disabled,
		} = this.props;

		return (
			<div ref={ (c) => { this.node = c; } } className="card--spacer">
				<div
					ref={ (c) => { this.card = c; } }
					className={ `card ${disabled ? 'card--disabled' : ''}` }
					onTouchStart={ this.startAction }
					onMouseDown={ this.startAction }
				>
					<h2 className="card-title">{ title }</h2>
					{ children }
				</div>
			</div>
		);
	}
}

export default Card;
