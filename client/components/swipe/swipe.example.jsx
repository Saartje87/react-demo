import React, { Component } from 'react';

import Swipe from './Swipe';

class SwipeExample extends Component {
	render() {
		return (
			<Swipe index={ 2 }>
				<div>One<hr /></div>
				<div>Two<hr /></div>
				<div>Three<hr /></div>
				<div>Four<hr /></div>
				<div>1. One<hr /></div>
				<div>1. Two<hr /></div>
				<div>1. Three<hr /></div>
				<div>1. Four<hr /></div>
			</Swipe>
		);
	}
}

export default SwipeExample;
