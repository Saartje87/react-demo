import React, { Component, Children, PropTypes } from 'react';
import './swipe.css';

const SCROLL_THRESHOLD = 16;
const SWIPE_THRESHOLD = 6;
const MOVES_MAX_LENGTH = 10;
// px per ? speed to determine fastswipe
const FAST_SWIPE_THRESHOLD = 0.6;
// Percentage of container to recognize swipe
const NORMAL_SWIPE_THRESHOLD = 0.4;

class Swipe extends Component {
	static propTypes = {
		className: PropTypes.string,
		children: PropTypes.node,
		index: PropTypes.number,
		onIndexChange: PropTypes.func,
		disabled: PropTypes.bool,
	};

	static defaultProps = {
		index: 0,
		className: '',
		disabled: false,
	};

	state = {
		width: 0,
	};

	children = [];
	current: { x: 0, y: 0 };
	delta = { x: 0, y: 0 };
	index = 0;
	isFirstRender = true;
	isSwiping = false;
	moves = [];
	start = { x: 0, y: 0 };

	componentWillMount() {
		window.addEventListener('resize', this.resizeAction);
	}

	componentDidMount() {
		this.index = this.props.index || 0;
		this.updateWidth();
	}

	componentWillReceiveProps(nextProps) {
		// Width only has to be updated when amount of children changes
		if (Children.count(nextProps.children) !== Children.count(this.props.children)) {
			this.updateWidth();
		}

		if (nextProps.index !== this.props.index) {
			this.animate(nextProps.index, 400);
			this.index = nextProps.index;
		}
	}

	componentDidUpdate() {
		this.slides = Array.from(this.swipeNode.children);
		this.slidesCount = this.slides.length;

		// On first render, update to correct index + position
		if (this.isFirstRender && this.state.width) {
			this.index = this.props.index || 0;
			this.setPosition(0);
			this.isFirstRender = false;
		}
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.resizeAction);
	}

	addListeners() {
		document.addEventListener('touchmove', this.moveAction);
		document.addEventListener('mousemove', this.moveAction);
		document.addEventListener('touchend', this.endAction);
		document.addEventListener('mouseup', this.endAction);
	}

	removeListeners() {
		document.removeEventListener('touchmove', this.moveAction);
		document.removeEventListener('mousemove', this.moveAction);
		document.removeEventListener('touchend', this.endAction);
		document.removeEventListener('mouseup', this.endAction);
	}

	resizeAction = () => {
		this.updateWidth();
		this.setPosition(0);
	}

	startAction = (event) => {
		if (event.defaultPrevented || this.props.disabled) {
			return;
		}

		this.moves.length = 0;
		this.index = this.getIndex(event);
		this.animate(this.index, 0);
		this.start = this.getEventXY(event);

		this.addListeners();
	}

	getIndex(event) {
		let node = event.target;

		do {
			if (node.hasAttribute('data-index')) {
				return parseInt(node.getAttribute('data-index'), 10);
			}
		} while (node = node.parentNode);

		return 0;
	}

	moveAction = (event) => {
		const { x, y } = this.getEventXY(event);
		const deltaX = x - this.start.x;
		const deltaY = y - this.start.y;

		if (!this.isSwiping) {
			// Bail out, user is scrolling
			if (Math.abs(deltaY) > SCROLL_THRESHOLD) {
				this.removeListeners();
				return;
			}

			// Wait a bit to be sure this is meant as a swipe event
			if (Math.abs(deltaX) < SWIPE_THRESHOLD) {
				return;
			}

			this.isSwiping = true;
			this.shouldUpdate = true;
			this.setWillChange(true);
			requestAnimationFrame(this.update);
		}

		// Disable scroll
		event.preventDefault();

		this.trackMove(event);
	}

	endAction = () => {
		if (this.isSwiping) {
			event.preventDefault();
			event.stopPropagation();
		}

		this.removeListeners();

		this.isSwiping = false;
		this.shouldUpdate = false;
	}

	trackMove(event) {
		const { x, y } = this.getEventXY(event);
		const deltaX = x - this.start.x;
		const deltaY = y - this.start.y;

		this.current = { x, y };

		// TODO
		this.moves.push({
			x: deltaX,
			y: deltaY,
			time: Date.now(),
			diffX: this.moves.length ? deltaX - this.moves[0].x : 0,
		});

		if (this.moves.length > MOVES_MAX_LENGTH) {
			this.moves.shift();
		}
	}

	update = () => {
		this.delta.x = this.current.x - this.start.x;

		// Friction
		if ((this.index === 0 && this.delta.x > 0) || (this.index === this.slidesCount - 1 && this.delta.x < 0)) {
			this.delta.x /= (Math.abs(this.delta.x) / this.state.width) + 1;
		}

		this.setPosition(this.delta.x);

		if (this.shouldUpdate) {
			requestAnimationFrame(this.update);
			return;
		}

		// User interaction ended
		this.setWillChange(false);

		// Last update call..
		const swipe = this.getSwipeInfo();

		if (swipe.isValid) {
			const count = this.slidesCount;
			const newIndex = Math.min(count - 1, Math.max(0, this.index + swipe.direction));

			this.animate(newIndex, Math.min(400, 300 / swipe.speed));
			this.index = newIndex;

			if (this.props.onIndexChange) {
				this.props.onIndexChange(newIndex);
			}
		} else {
			this.animate(this.index, 300);
		}
	}

	setPosition(x) {
		const transform = `translateX(${-(this.state.width * this.index) + x}px)`;
		// On first render, update all slides till given index
		const min = this.isFirstRender ? 0 : this.index - 1;
		const max = this.isFirstRender ? this.index : this.index + 1;

		for (let i = min; i <= max; i++) {
			const node = this.slides[i];

			if (node) {
				node.style.transform = transform;
				node.style.webkitTransform = transform;
			}
		}
	}

	animate(newIndex, speed = 500) {
		const transform = `translateX(-${newIndex * this.state.width}px)`;
		const transitionDuration = `${speed}ms`;
		const min = Math.min(newIndex, this.index) - 1;
		const max = Math.max(newIndex, this.index) + 1;

		for (let i = min; i <= max; i++) {
			const node = this.slides[i];

			if (node) {
				node.style.transitionDuration = transitionDuration;
				node.style.webkitTransitionDuration = transitionDuration;

				if (speed !== 0) {
					node.style.transform = transform;
					node.style.webkitTransform = transform;
				}
			}
		}
	}

	setWillChange(willChange = true) {
		const min = this.index - 1;
		const max = this.index + 1;

		for (let i = min; i <= max; i++) {
			const node = this.slides[i];

			if (node) {
				node.style.willChange = willChange ? 'transform' : '';
			}
		}
	}

	getSwipeInfo() {
		if (!this.moves.length) {
			return { isValid: false };
		}

		const moves = this.moves;
		const total = moves.reduce((total, { diffX }) => total + diffX, 0);
		const averageX = Math.abs(total / moves.length);
		const lastMove = moves[moves.length - 1];
		const time = lastMove.time - moves[0].time;
		const speed = averageX / time;
		const info = {
			isValid: true,
			direction: (lastMove.x < 0) ? 1 : -1,
			speed, // TODO total ??
		};

		// Fast swipe
		if (speed > FAST_SWIPE_THRESHOLD) {
			return info;
		}

		// Normal swipe
		if (Math.abs(lastMove.x) > (this.state.width * NORMAL_SWIPE_THRESHOLD)) {
			return info;
		}

		return { isValid: false };
	}

	getEventXY(event) {
		const touches = event.touches ? event.touches[0] : event;

		return {
			x: touches.pageX,
			y: touches.pageY,
		};
	}

	updateWidth() {
		if (this.swipeNode && this.swipeNode.parentNode) {
			const swipe = this.swipeNode.parentNode.getBoundingClientRect();

			this.setState({
				width: swipe.width,
			});
		}
	}

	render() {
		const {
			className,
			children,
		} = this.props;

		return (
			<div
				className={ `swipe ${className}` }
				onTouchStart={ this.startAction }
				onMouseDown={ this.startAction }
			>
				<div
					className="swipe-container"
					ref={ (node) => { this.swipeNode = node; } }
					style={ {
						width: `${this.state.width * Children.count(children)}px`,
					} }
				>
					{ Children.map(children, (component, index) =>
						<div
							className="swipe-panel"
							key={ index }
							data-index={ index }
						>
							{ component }
						</div>,
					) }
				</div>
			</div>
		);
	}
}

export default Swipe;
