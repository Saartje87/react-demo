# Tabs

## Usage

```js
import React, { Component } from 'react';
import Swipe from 'components/swipe';

class SwipeExample extends Component {
	state = {
		index: 0,
	};
	
	render() {
		return (
			<Swipe>
				<div>Panel 1</div>
				<div>Panel 2</div>
				<div>Panel 3</div>
				<div>Panel 4</div>
			</Swipe>
		);
	}
}
```

### Tabs

#### Properties

| Name | Type | Default | Description |
| - | - | - | - |
| className | String | '' | Additional class name to provide custom styling. |
| index | Number | 0 | Set active slide |
| onIndexChange | Function | | Callback function that is fired when the slide changes. |
| disabled | Boolean | false | Disable swipe |
