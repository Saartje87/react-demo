import React, { Component } from 'react';

import './about.css';

class About extends Component {
	render() {
		return (
			<div className="about">
				<h1>Swipe me!</h1>

				<h2>Credits</h2>

				<ul>
					<li>Foo</li>
					<li>Bar</li>
					<li>Hello world!</li>
				</ul>

				<h2>Wockels</h2>

				<p>Lorum wil een <strong>cookie</strong>. Lorum wil een <i>cookie</i>. Lorum wil een cookie. Lorum wil een cookie. Lorum wil een cookie. Lorum wil een cookie. </p>

				<h3>El image</h3>

				<img src="http://www.shauntmax30.com/data/out/10/1011657-darkwing-duck-desktop-wallpapers.png" alt="Darkwing Duck" />
			</div>
		);
	}
}

export default About;
