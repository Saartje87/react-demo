import React, { Component, PropTypes } from 'react';

import './swipe.css';

/*
<Swipe index={ 0 } onChange={ index => console.log('New index: ', index) }>
	<div>Slide 1</div>
	<div>Slide 2</div>
</Swipe>
*/

class Swipe extends Component {
	static propTypes = {
		className: PropTypes.string,
		children: PropTypes.node,
		index: PropTypes.number,
		onChange: PropTypes.func,
	};

	state = {
		width: 0,
	};

	index = 0;
	isDragging = false;
	newPos = 0;
	queuedIndex = 0;
	rAF = null;
	slide = null;
	slides = [];

	startAction = ::this.startAction;
	moveAction = ::this.moveAction;
	endAction = ::this.endAction;
	update = ::this.update;

	componentDidMount() {
		this.setSize();
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.index !== nextProps.index && nextProps.index !== this.queuedIndex) {
			const position = -(this.state.width * nextProps.index);
			this.setPosition(nextProps.index - 1, position);
			this.setPosition(nextProps.index, position);
			this.setPosition(nextProps.index + 1, position);

			this.index = nextProps.index;
		}
	}

	componentWillUpdate() {
		// Empty slides array
		this.slides.length = 0;
	}

	setSize() {
		const {
			width,
		} = this.swipe.getBoundingClientRect();

		if (width !== this.state.width) {
			this.setState({ width });
		}
	}

	addListeners() {
		document.addEventListener('touchmove', this.moveAction);
		document.addEventListener('mousemove', this.moveAction);
		document.addEventListener('touchend', this.endAction);
		document.addEventListener('mouseup', this.endAction);
	}

	removeListeners() {
		document.removeEventListener('touchmove', this.moveAction);
		document.removeEventListener('mousemove', this.moveAction);
		document.removeEventListener('touchend', this.endAction);
		document.removeEventListener('mouseup', this.endAction);
	}

	// TODO Fast swipe will wrongly set index?
	startAction(event) {
		if (event.defaultPrevented) {
			return;
		}

		this.newPos = 0;
		this.index = this.queuedIndex;
		this.start = {
			x: (event.pageX || event.touches[0].pageX),
			y: (event.pageY || event.touches[0].pageY),
			time: Date.now(),
		};
		this.current = { ...this.start };
		this.delta = { x: 0, y: 0 };

		this.addListeners();
	}

	moveAction(event) {
		if (event.touches && event.touches.length > 1) {
			return;
		}

		const xy = event.touches ? event.touches[0] : event;

		// This event chain is not meant for us :)
		if (!this.isDragging && Math.abs(xy.pageY - this.start.y) > 10) {
			this.removeListeners();
			return;
		}

		// Activate dragging after a fixed amount of movement
		if (!this.isDragging && Math.abs(xy.pageX - this.start.x) < 8) {
			return;
		}

		if (!this.isDragging) {
			this.isDragging = true;
			this.willChange(true);
			requestAnimationFrame(this.update);
		}

		// Disable scroll
		event.preventDefault();

		this.current = {
			x: xy.pageX,
			y: xy.pageY,
		};
	}

	endAction() {
		this.isDragging = false;
		this.removeListeners();

		const deltaX = this.delta.x;
		const deltaTime = Date.now() - this.start.time;

		// console.log(deltaX, this.current.x - this.start.x, deltaTime);

		if (Math.abs(deltaX) < 25 || deltaTime < 50) {
			return;
		}

		// TODO We should track last 5 updates or move changes to determine slide speed and
		// if slide should move to next/prev

		// console.log(0.3 * Math.min(1, deltaTime / 1000), Math.min(1, deltaTime / 1000));
		// console.log(Math.abs(deltaX), this.state.width * (0.3 * Math.min(1, deltaTime / 500)));
		// console.log(this.delta.x / deltaTime);

		this.queuedIndex = (deltaX > 0) ? Math.max(0, this.index - 1) : Math.min(this.slides.length - 1, this.index + 1);
		this.newPos = -((this.queuedIndex - this.index) * this.state.width);
	}

	willChange(transform) {
		[-1, 0, 1].forEach((i) => {
			const slide = this.slides[this.index + i];

			if (slide) {
				slide.style.willChange = transform ? 'transform' : '';
			}
		});
	}

	update() {
		const deltaX = Math.round(this.delta.x);
		this.rAF = requestAnimationFrame(this.update);

		if (!this.isDragging) {
			this.delta.x += (this.newPos - this.delta.x) / 4;

			// The /4 trick wont get to the end value..
			if (Math.abs(this.newPos - this.delta.x) < 3) {
				this.delta.x = this.newPos;
			}
		} else {
			this.delta.x = this.current.x - this.start.x;

			// Friction
			if ((this.index === 0 && this.delta.x > 0) || (this.index === this.slides.length - 1 && this.delta.x < 0)) {
				this.delta.x /= (Math.abs(this.delta.x) / this.state.width) + 1;
			}
		}

		this.setPosition(this.index - 1, this.delta.x);
		this.setPosition(this.index, this.delta.x);
		this.setPosition(this.index + 1, this.delta.x);

		// TODO Remove 'hackish' deltaX compersion
		if (!this.isDragging && Math.round(this.delta.x) === deltaX) {
			cancelAnimationFrame(this.rAF);
			this.willChange(false);
			this.index = this.queuedIndex;

			if (this.props.onChange) {
				this.props.onChange(this.index);
			}
		}
	}

	setPosition(index, x) {
		const slide = this.slides[index];

		if (!slide) {
			return;
		}

		slide.style.transform = `translateX(${x - (this.index * this.state.width)}px)`;
		slide.style.webkitTransform = `translateX(${x - (this.index * this.state.width)}px)`;
	}

	// TODO Public api (next/prev/setSlide/...)

	render() {
		const {
			className,
			children,
		} = this.props;
		const {
			width,
		} = this.state;

		return (
			<div
				ref={ (c) => { this.swipe = c; } }
				className={ `swipe ${className || ''}` }
			>
				<div
					className={ `swipe--container` }
					style={ { width: width * React.Children.count(children) } }
					onTouchStart={ this.startAction }
					onMouseDown={ this.startAction }
				>
					{ React.Children.map(children, (component, key) => (
						<div
							className="swipe--item"
							key={ key }
							ref={ (c) => { this.slides[key] = c; } }
							data-index={ key }
						>
							{ component }
						</div>
					)) }
				</div>
			</div>
		);
	}
}

export default Swipe;
