import React, { Component, PropTypes } from 'react';

import './timer.css';

class Timer extends Component {
	static propTypes = {
		duration: PropTypes.number.isRequired,
		onEnded: PropTypes.func.isRequired,
	};

	state = {
		secondsLeft: 0,
	};

	componentWillMount() {
		this.start = Date.now();

		this.timer = setInterval(::this.update, 1000);
		this.update();
	}

	componentWillUnmount() {
		clearTimeout(this.timer);
	}

	timer = null;
	start = null;

	end() {
		clearTimeout(this.timer);
		this.props.onEnded();
	}

	update() {
		const {
			duration,
		} = this.props;
		const secondsPassed = Math.floor((Date.now() - this.start) / 1000);
		const secondsLeft = Math.max(0, duration - secondsPassed);

		this.setState({
			secondsLeft,
		});

		if (!secondsLeft) {
			this.end();
		}
	}

	render() {
		const {
			secondsLeft,
		} = this.state;

		return (
			<div className={ `timer ${secondsLeft <= 10 ? 'timer--panic' : ''}` }>
				{ secondsLeft }
			</div>
		);
	}
}

export default Timer;
