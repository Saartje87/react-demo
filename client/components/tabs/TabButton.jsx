import React, { Component, PropTypes } from 'react';

class TabButton extends Component {
	static propTypes = {
		label: PropTypes.node.isRequired,
		active: PropTypes.bool,
		onClick: PropTypes.func,
		index: PropTypes.number,
	};

	clickAction = () => {
		if (this.props.onClick) {
			this.props.onClick(this.props.index);
		}
	}

	render() {
		const {
			active,
			label,
		} = this.props;

		return (
			<button
				className={ `tabs-header-button ${active ? 'is-active' : ''}` }
				onClick={ this.clickAction }
				role="tab"
				aria-selected={ String(active) }
			>
				{ label }
			</button>
		);
	}
}

export default TabButton;
