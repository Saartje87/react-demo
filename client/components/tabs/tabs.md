# Tabs

## Usage

```js
import React, { Component } from 'react';
import { Tab, Tabs } from 'components/tabs';

class TabsExample extends Component {
	state = {
		index: 0,
	};
	
	render() {
		return (
			<Tabs index={ this.state.index } onIndexChange={ index => this.setState({ index }) }>
				<Tab label="First">First content</Tab>
				<Tab label="Second">Second content</Tab>
			</Tabs>
		);
	}
}
```

### Tabs

#### Properties

| Name | Type | Default | Description |
| - | - | - | - |
| className | String | '' | Additional class name to provide custom styling. |
| index | Number | | Active tab |
| onIndexChange | Function | | Callback function that is fired when the tab changes. |
| swipe | Boolean | true | Tab panels are swipeable |

### Tab

#### Properties

| Name | Type | Default | Description |
| - | - | - | - |
| className | String | '' | Additional class name to provide custom styling. |
| label | String | | Label text for navigation header. |
| children | Node | | Tab panel contents |
