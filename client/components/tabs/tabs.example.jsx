import React, { Component } from 'react';
import { Tab, Tabs } from 'components/tabs';
import Swipe from 'components/swipe';

class TabsExample extends Component {
	state = {
		index: 0,
		swipeIndex: 1,
	};

	render() {
		return (
			<div>
				<Tabs
					index={ this.state.index }
					onIndexChange={ index => this.setState({ index }) }
					swipe={ false }
				>
					<Tab label="Tab 1"><h1>Tab1 content</h1><p>Lorum cookie</p></Tab>
					<Tab label="Tab 2"><h1>Tab2 content</h1><p>Lorum cookie</p></Tab>
					<Tab label="Tab 3 tab tab"><h1>Tab3 content</h1><p>Lorum cookie</p></Tab>
				</Tabs>

				<Tabs
					index={ this.state.swipeIndex }
					onIndexChange={ swipeIndex => this.setState({ swipeIndex }) }
				>
					<Tab label="A"><h1>Swipe 1</h1><p>Lorum cookie</p></Tab>
					<Tab label="BB"><h1>Swipe 2</h1><p>Lorum cookie</p></Tab>
					<Tab label="CCCC"><h1>Swipe 3</h1><p>Lorum cookie</p></Tab>
					<Tab label="D"><h1>Swipe 4</h1><p>Lorum cookie</p></Tab>
					<Tab label="EEE"><h1>Swipe 5</h1><p>Lorum cookie</p></Tab>
				</Tabs>
			</div>
		);
	}
}

export default TabsExample;
