import React, { Component, PropTypes } from 'react';

import './tabs.css';
import Tab from './Tab';
import TabButton from './TabButton';
import TabPanel from './TabPanel';
import Swipe from 'components/swipe';

class Tabs extends Component {
	static propTypes = {
		className: PropTypes.string,
		children: PropTypes.node.isRequired,
		index: PropTypes.number.isRequired,
		onIndexChange: PropTypes.func.isRequired,
		swipe: PropTypes.bool,
	};

	static defaultProps = {
		swipe: true,
		className: '',
	};

	state = {
		bar: {
			left: 0,
			width: 0,
		},
	};

	componentWillMount() {
		this.parseChildren();
	}

	componentDidMount() {
		this.updateBar(this.props.index);
	}

	componentWillReceiveProps(nextProps) {
		// Layout changes got triggered in react chain..
		requestAnimationFrame(() => this.updateBar(nextProps.index));
	}

	componentWillUpdate({ children }) {
		this.parseChildren(children);
	}

	setIndex = (index) => {
		if (this.props.onIndexChange) {
			this.props.onIndexChange(index);
		}
	};

	updateBar(index) {
		if (this.navigationNode && this.navigationNode.children[index]) {
			const nav = this.navigationNode.getBoundingClientRect();
			const button = this.navigationNode.children[index].getBoundingClientRect();

			this.setState({
				bar: {
					left: `${button.left - nav.left}px`,
					width: `${button.width}px`,
				},
			});
		}
	}

	parseChildren(children = null) {
		const headers = [];
		const contents = [];

		React.Children.forEach(children || this.props.children, (item) => {
			if (item.type === Tab) {
				headers.push(<TabButton label={ item.props.label } />);
				contents.push(<TabPanel children={ item.props.children } />);
			} else {
				console.warn(`Unexpected item `, item.type);
			}
		});

		this.contents = contents;
		this.headers = headers;
	}

	renderHeaders() {
		return this.headers.map((item, index) =>
			React.cloneElement(item, {
				id: `tab-header-${index}`,
				key: index,
				active: this.props.index === index,
				onClick: this.setIndex,
				index,
			}),
		);
	}

	renderContents() {
		return this.contents.map((item, index) =>
			React.cloneElement(item, {
				key: index,
				active: this.props.index === index,
			}),
		);
	}

	render() {
		return (
			<div className={ `tabs ${this.props.className}` }>
				<div
					ref={ (node) => { this.navigationNode = node; } }
					role="tablist"
					className="tabs-header"
				>
					{ this.renderHeaders() }
					<span className="tabs-header-bar" style={ this.state.bar } />
				</div>
				<Swipe
					ref={ (node) => { this.panelNode = node; } }
					className="tabs-main"
					index={ this.props.index }
					onIndexChange={ this.props.onIndexChange }
					disabled={ !this.props.swipe }
				>
					{ this.renderContents() }
				</Swipe>
			</div>
		);
	}
}

export default Tabs;
