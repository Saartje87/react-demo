import React, { Component, PropTypes } from 'react';

class TabPanel extends Component {
	static propTypes = {
		children: PropTypes.node,
		active: PropTypes.bool,
	};

	render() {
		const {
			active,
			children,
		} = this.props;

		return (
			<section
				className={ `tabs-panel ${active ? 'is-active' : ''}` }
				role="tabpanel"
				aria-hidden={ String(!active) }
			>
				{ children }
			</section>
		);
	}
}

export default TabPanel;
