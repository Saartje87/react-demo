import React from 'react';
import TestUtils from 'react-addons-test-utils';
import Page from './Page';

describe('Page', () => {
	it('should not be undefined', () => {
		const Component = TestUtils.renderIntoDocument(<Page />);
		const element = TestUtils.findRenderedDOMComponentWithTag(Component, 'div');

		expect(element).toBeTruthy();
	});
});
