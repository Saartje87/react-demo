import React, { Component, PropTypes } from 'react';

import './page.css';

class Page extends Component {
	static propTypes = {
		children: PropTypes.node,
	};

	render() {
		const {
			children,
		} = this.props;

		return (
			<div className="page">
				{ children }
			</div>
		);
	}
}

export default Page;
