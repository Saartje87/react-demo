import React, { Component, PropTypes } from 'react';

import './interaction.css';
import Card from 'components/card';
// import Timer from 'components/timer';

class Interaction extends Component {
	static propTypes = {
		interaction: PropTypes.shape({
			interactions: PropTypes.arrayOf(PropTypes.shape({
				id: PropTypes.number.isRequired,
				title: PropTypes.string.isRequired,
			})),
		}),
		submitAnswer: PropTypes.func.isRequired,
	};

	state = {
		timerEnded: false,
	};

	onCardDismiss(id, answer) {
		this.props.submitAnswer({
			id,
			answer: answer === 'yes',
		});
	}

	render() {
		const {
			interaction: { meta, interactions },
		} = this.props;
		const {
			timerEnded,
		} = this.state;

		return (
			<div className="interaction">
				{/* Map interactions to cards */}
				{ interactions
					.map(({ id, title }) => (
						<Card
							key={ `${id}-${(meta[id] === undefined) ? '0' : '1'}` }
							disabled={ timerEnded || (meta[id] !== undefined) }
							onDismiss={ (meta[id] !== undefined) ? () => {} : answer => this.onCardDismiss(id, answer) }
							title={ title }
						>
							{ (meta[id] !== undefined) ?
								<div>
									{ `You answered ${meta[id] ? 'YES' : 'NO'}` }
								</div> :
								<div>
									{ `YES -->` }
									<br />
									{ `<-- NO` }
								</div> }

						</Card>
				)) }

				{/*<Timer duration={ 30 } onEnded={ () => this.setState({ timerEnded: true }) } />*/}
			</div>
		);
	}
}

export default Interaction;
