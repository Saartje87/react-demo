import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

import './input.css';

class Input extends Component {
	static propTypes = {
		label: PropTypes.string.isRequired,
		onChange: PropTypes.func,
	};

	state = {
		open: false,
		value: '',
	};

	onChange(event) {
		const { onChange } = this.props;
		const { value } = event.target;

		this.setState({ value });

		if (onChange) {
			onChange(value);
		}
	}

	onKeyPress(event) {
		const {
			onRequestClose,
		} = this.props;

		// Enter
		// TODO Handle ios done button /cry
		if (event.which === 13) {
			this.setState({ open: false });
			this.onChange(event);
		}
	}

	render() {
		const {
			label,
		} = this.props;

		const {
			value,
			open,
		} = this.state;

		return (
			<div className="input" tabIndex="-1">
				<label className="input-label" onClick={ () => this.setState({ open: true }) }>{ value || label }</label>

				{/* Mhh, `<foo open={ open }>` or `open && <foo>` */}
				{/* Performance impact will decide winner */}
				{open &&
					<Overlay onRequestClose={ () => this.setState({ open: false }) }>
						<div>
							<h2>
								<label htmlFor="input">{ `Enter your ${label}` }</label>
							</h2>
							<input autoFocus id="input" type="text" defaultValue={ value } onKeyPress={ ::this.onKeyPress } />
						</div>
					</Overlay> }
			</div>
		);
	}
}

class Overlay extends Component {
	componentWillLeave(done) {
		const {
			self,
		} = this.refs;

		self.classList.add('leave');

		setTimeout(done, 500);
	}

	onClose(event) {
		const {
			onRequestClose,
		} = this.props;

		if (event.target === this.refs.self) {
			onRequestClose();
		}
	}

	render() {
		const {
			children,
		} = this.props;

		return (
			<div ref="self" className="input-overlay" onClick={ ::this.onClose }>
				{ children }
			</div>
		);
	}
}

function getDisplayName(Component) {
	return Component.displayName || Component.name || 'Component';
}

const injectFoo = (WrappedComponent) => {
	class InjectFoo extends Component {
		static displayName = `InjectFoo(${getDisplayName(WrappedComponent)})`;
		static WrappedComponent = WrappedComponent;

		static propTypes = {
			children: PropTypes.element.isRequired,
		};

		_layer = null;
		_component = null;

		// Bind class method to `this`
		onLeaveDone = ::this.onLeaveDone;

		componentDidMount() {
			this.renderLayer();
		}

		componentDidUpdate() {
			this.renderLayer();
		}

		componentWillUnmount() {
			this.unrenderLayer();
		}

		renderLayer() {
			const component = <WrappedComponent { ...this.props } />;

			this.createLayer();

			this._component = ReactDOM.unstable_renderSubtreeIntoContainer(this, component, this._layer);
		}

		createLayer() {
			if (this._layer) {
				return;
			}

			this._layer = document.createElement('div');
			document.body.appendChild(this._layer);
		}

		unrenderLayer() {
			if (!this._component) {
				return;
			}

			if (this._component.componentWillLeave) {
				this._component.componentWillLeave(this.onLeaveDone);
			} else {
				this.onLeaveDone();
			}

			this._component = null;
		}

		onLeaveDone() {
			if (!this._layer) {
				return;
			}

			ReactDOM.unmountComponentAtNode(this._layer);
			document.body.removeChild(this._layer);

			this._layer = null;
		}

		render() {
			return null;
		}
	}

	return InjectFoo;
};

Overlay = injectFoo(Overlay);

export default Input;
