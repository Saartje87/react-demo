import React from 'react';
import TestUtils from 'react-addons-test-utils';
import Input from './Input';

describe('Input', () => {
	it('should not be undefined', () => {
		const Component = TestUtils.renderIntoDocument(<Input />);
		const element = TestUtils.findRenderedDOMComponentWithTag(Component, 'div');

		expect(element).toBeTruthy();
	});
});
