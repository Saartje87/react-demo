import React, { Component, PropTypes } from 'react';
import ReactSwipe from 'react-swipe';
import { Match } from 'react-router';

import './swipe.css';

class Swipe extends Component {
	static propTypes = {
		children: PropTypes.arrayOf(PropTypes.node).isRequired,
		className: PropTypes.string,
	};

	update(index) {
		if (!this.reactSwipe) {
			requestAnimationFrame(() => this.update(index));
		} else {
			this.reactSwipe.slide(index);
		}

		return null;
	}

	render() {
		const {
			children,
			className,
		} = this.props;

		const swipeOptions = {
			continuous: false,
			disableScroll: false,
		};

		return (
			<ReactSwipe
				ref={ (el) => { this.reactSwipe = el; } }
				className={ `swipe ${className || ''}` }
				swipeOptions={ swipeOptions }
			>
				{ children.map((child, index) => (
					<div key={ index }>
						{ child }
						<Match exactly={ child.props.exactly } pattern={ child.props.pattern } render={ () => this.update(index) } />
					</div>
				)) }
			</ReactSwipe>
		);
	}
}

export default Swipe;
