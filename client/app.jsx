// Polyfills
import 'core-js/fn/array/from';
import 'web-animations-js';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import App from 'containers/app';
import { configureStore } from 'config/store';
import Swipe from 'components/swipe';
import TabsExample from 'components/tabs/tabs.example';
import SwipeExample from 'components/swipe/swipe.example';

const store = configureStore();

// TODO swipe={ true }
// render(
// 	<SwipeExample />
// , document.getElementById('mount'));
// render(
// 	<TabsExample />
// , document.getElementById('mount'));

render(
	<Provider store={ store }>
		<App />
	</Provider>
, document.getElementById('mount'));
