import {
	UPDATE_ANSWER,
} from 'actions/interaction';

export const initialState = {
	meta: {},
	interactions: [
		{
			id: 1,
			title: 'First card',
		},
		{
			id: 2,
			title: 'Second card',
		},
		{
			id: 3,
			title: 'Third card',
		},
	],
};

export const interaction = (state = initialState, { type, payload }) => {
	switch (type) {
		case UPDATE_ANSWER:
			return {
				...state,
				meta: {
					...state.meta,
					[payload.id]: payload.answer,
				},
			};

		default:
			return state;
	}
};
