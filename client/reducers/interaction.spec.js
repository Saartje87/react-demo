import {
	UPDATE_ANSWER,
} from 'actions/interaction';

import {
	initialState,
	interaction as reducer,
} from './interaction';

describe('Interaction reducer', () => {
	it('should return the initial state', () => {
		expect(
			reducer(undefined, {}),
		).toEqual(initialState);
	});

	it('should update an answer', () => {
		expect(
			reducer(undefined, {
				type: UPDATE_ANSWER,
				payload: {
					id: 1,
					answer: false,
				},
			}),
		).toEqual({
			...initialState,
			meta: { 1: false },
		});
	});
});
