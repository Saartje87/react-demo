import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';

import * as reducers from 'reducers';

export const configureStore = (initialState = {}) => {
	const enhancer = compose(
		applyMiddleware(thunk),
		window.devToolsExtension ? window.devToolsExtension() : (f => f),
	);

	const rootReducer = combineReducers({
		...reducers,
	});

	const store = createStore(
		rootReducer,
		initialState,
		enhancer,
	);

	if (module.hot) {
		module.hot.accept('../reducers', () => {
			/* eslint global-require: "off" */
			const nextRootReducer = require('../reducers');

			store.replaceReducer(nextRootReducer);
		});
	}

	return store;
};
