# README

eslint

Importeer EXACT! wat je nodig heb!
Decorators

HoC
- Lijnlaag, do some research

Components
- Herbruikbaar, niet met redux koppelen.
- Vertalingen, const messages = defineMessages(), formatMessage into 
- Alleen jsx in de render functie
- PropTypes!
- Inprencipe moet een component altijd plug and play zijn

Containers
- Aansluiting met redux / routing / oid..
- Triggers actions (and handle async)

Api

css
- `${ComponentName}--${name}`
- Theming?

Unit testing
- Wallaby

Actions
- Const: `${@actionName}/${constantName}`
- Action itself: 
	- Always return the promise.
	- Every method must be testable (so no .then(() => { Do random stuff which cannot be tested }))

Reducers
- Immutable! (es6, immutablejs is not really required to archieve this)
- meta & collection
- initialState, default values!

Analytics
- Idea: Use redux middleware!

Selectors
- ???

Async actions aka Promises, return values vastleggen (then / catch, of then with error object)


TEST PROJECT, provisioning-client
