const cssnext = require('postcss-cssnext');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const postcssImport = require('postcss-import');
const postcssReporter = require('postcss-reporter');
const precss = require('precss');
const webpack = require('webpack');

const nodeEnv = process.env.NODE_ENV || 'dev';

module.exports = {
	// cheap-module-eval-source-map ?
	devtool: 'eval-source-map',
	performance: {
		hints: false,
	},
	context: path.join(__dirname, '../client'),
	output: {
		path: path.join(__dirname, '../dist'),
		filename: 'bundle.js',
	},
	resolve: {
		extensions: ['.js', '.jsx'],
		modules: [
			path.resolve('./client'),
			'node_modules',
		],
	},
	entry: {
		js: './app.jsx',
		vendor: [
			'react',
			'react-dom',
			'react-redux',
			'react-router',
			'redux',
			// 'redux-persist',
			'redux-thunk',
			// 'reselect',
			'web-animations-js',
		],
	},
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					// Mhh .babelrc + 'react-hmre'...
					presets: [['es2015', { modules: false }], 'stage-0', 'stage-1', 'react', 'react-hmre'],
					plugins: ['transform-decorators-legacy'],
					cacheDirectory: true,
					// sourceMap: true,
				},
			},
			{
				test: /\.css$/,
				exclude: /node_modules/,
				loaders: [
					'style-loader',
					'css-loader?sourceMap',
					'postcss-loader',
				],
			},
			{
				test: /\.html$/,
				loader: 'html-loader',
			},
			{
				test: /\.(png|jpg|gif)$/,
				// inline base64 URLs for <=8k images, direct URLs for the rest
				loader: 'url-loader?limit=8192',
			},
			{
				test: /\.json$/,
				loader: 'json-loader',
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: 'app.html',
			filename: 'index.html',
			hash: nodeEnv !== 'dev',
		}),
		new webpack.NamedModulesPlugin(),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			minChunks: Infinity,
			filename: 'vendor.bundle.js',
		}),
		new webpack.LoaderOptionsPlugin({
			minimize: false,
			debug: true,
			options: {
				context: path.resolve('./client'),
				postcss: function postcssHander() {
					return [
						postcssImport(),
						cssnext(),
						postcssReporter({ clearMessages: true }),
						precss(),
					];
				},
			},
		}),
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify(nodeEnv),
			},
		}),
	],
};
