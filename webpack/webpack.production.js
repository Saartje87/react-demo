throw 'Exit';

const webpack = require('webpack');
const path = require('path');

const cssnext = require('postcss-cssnext');
const postcssReporter = require('postcss-reporter');

// Plugins
const HtmlWebpackPlugin = require('html-webpack-plugin');

const nodeEnv = process.env.NODE_ENV || 'dev';

module.exports = {
	devtool: 'hidden-source-map',
	context: path.join(__dirname, '../client'),
	output: {
		path: 'dist',
		filename: 'bundle.js',
	},
	resolve: {
		extensions: ['.js', '.jsx'],
		modules: [
			path.resolve('./client'),
			'node_modules',
		],
	},
	entry: {
		js: './app.jsx',
	},
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: [['es2015', { modules: false }], 'stage-0', 'stage-1', 'react'],
					plugins: ['transform-decorators-legacy'],
					cacheDirectory: true,
				},
			},
			{
				test: /\.css$/,
				loader: 'style-loader!css-loader!postcss-loader',
			},
			{
				test: /\.html$/,
				loader: 'html-loader',
			},
		],
	},
	plugins: [
		new webpack.NamedModulesPlugin(),
		new HtmlWebpackPlugin({
			template: './app.html',
			filename: 'index.html',
			hash: nodeEnv !== 'dev',
		}),
		new webpack.LoaderOptionsPlugin({
			minimize: true,
			debug: false,
			options: {
				postcss: [
					cssnext(),
					postcssReporter({ clearMessages: true }),
				],
			},
		}),
		// new webpack.optimize.UglifyJsPlugin({
		// 	compress: {
		// 		warnings: false,
		// 	},
		// 	output: {
		// 		comments: false,
		// 	},
		// 	sourceMap: false,
		// }),
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify(nodeEnv),
			},
		}),
	],
};
